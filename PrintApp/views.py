from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify

from PrintApp.models import Images
from PrintApp.serializers import ImageSerializer


from django.core.files.storage import default_storage

# Create your views here.
# @receiver(pre_save)
# def my_callback(sender, instance, *args, **kwargs):
#     instance.slug = slugify(instance.title)
#
@csrf_exempt
def imagesApi(request,id=0):
    if request.method=='GET':
        images = Images.objects.all()
        images_serializer = ImageSerializer(images, many=True)
        return JsonResponse(images_serializer.data,safe=False)
    elif request.method=='POST':
        image_data=JSONParser().parse(request)
        images_serializer = ImageSerializer(data=image_data)
        if images_serializer.is_valid():
            images_serializer.save()
            return JsonResponse("Added Successfully",safe=False)
        return JsonResponse("Failed to Add", safe=False)
    elif request.method=='PUT':
        image_data = JSONParser().parse(request)
        image = Images.objects.get(imgId=image_data['imgId'])
        images_serializer = ImageSerializer(image,data=image_data)
        if images_serializer.is_valid():
            images_serializer.save()
            return JsonResponse("Updated Successfully",safe=False)
        return JsonResponse("Failed to Update", safe=False)
    elif request.method=='DELETE':
        image = Images.objects.get(imgId=id)
        image.delete()
        return JsonResponse("Deleted Successfully",safe=False)

@csrf_exempt
def SaveFile(request):
    file = request.FILES['scan']
    file_name = default_storage.save(file.name,file)
    return JsonResponse(file_name,safe=False)

# def addImg(request):
#     print('asad')
#     if request.method == "POST":
#         img = Images()
#         img.imgId = request.POST.get('imgId')
#         img.printer = request.POST.get('printer')
#
#         if len(request.FILES) != 0:
#             img.image = request.FILES['scan']
#
#         img.save()
#         messages.success(request, "Product Added Successfully")
#         return redirect('/')
#     return render(request, 'products/add.html')