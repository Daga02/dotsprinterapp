from django.db import models

# Create your models here.

class Images(models.Model):
    imgId = models.AutoField(primary_key=True)
    scan = models.ImageField(upload_to='scan/')
    mask = models.ImageField(upload_to='mask/')
    printer = models.CharField(max_length=50, null=True)