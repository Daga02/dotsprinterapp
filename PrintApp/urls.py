from django.contrib import admin
from django.template.defaulttags import url
from django.urls import path, re_path
from django.conf import settings
from django.conf.urls.static import static
from PrintApp import views


urlpatterns = [
    re_path(r'^image/$',views.imagesApi, name='image'),
    re_path(r'^image/([0-9]+)$',views.imagesApi, name='image'),

    re_path(r'^image/savefile', views.SaveFile, name='image'),

]+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
